export async function responseFetch(
  url: string,
  method = "GET",
  body: any = null
) {
  const request = await fetch(url, {
    method: method ?? "GET",
    body,
  });

  if (request.status !== 200 && request.status === 301) {
    return { message: "Une erreur est survenue lors de l'appelle api." };
  }
  return request.json();
}
