import { responseFetch } from "../Fetch";

export async function listType(): Promise<string[]> {
  const response = await responseFetch("https://api.waifu.pics/endpoints");

  return response.sfw ?? [];
}
