import { responseFetch } from "../Fetch";

export async function post(type: string, category: string) {
  const formData = new FormData();
  formData.set("exclude", "[]");

  return await responseFetch(
    `https://api.waifu.pics/many/${type}/${category}`,
    "POST",
    formData
  );
}
