import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";

// @ts-ignore
import ClickOutside from "click-outside-vue3";

createApp(App).use(ClickOutside).mount("#app");
